-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 09, 2018 at 12:23 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `design_pattern`
--

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `detail` varchar(255) NOT NULL DEFAULT '',
  `price` int(11) NOT NULL,
  `teach_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `name`, `detail`, `price`, `teach_by`, `created_at`) VALUES
(1, 'Cooking', '', 90, 24, '2018-06-09 07:33:34'),
(2, 'Acting', '', 90, 23, '2018-06-09 07:33:34'),
(3, 'Chess', '', 90, 22, '2018-06-09 07:33:34'),
(4, 'Writing', '', 90, 21, '2018-06-09 07:33:34'),
(5, 'Conservation', '', 90, 20, '2018-06-09 07:33:34'),
(6, 'Tennis', '', 90, 19, '2018-06-09 07:33:34'),
(7, 'The Art of Performance', '', 90, 18, '2018-06-09 07:33:34'),
(8, 'Writing #2', '', 90, 17, '2018-06-09 07:33:34'),
(9, 'Building a Fashion Brand', '', 90, 16, '2018-06-09 07:33:34'),
(10, 'Design and Architecture', '', 90, 15, '2018-06-09 07:33:34'),
(11, 'Singing', '', 90, 14, '2018-06-09 07:33:34'),
(12, 'Jazz', '', 90, 13, '2018-06-09 07:33:34'),
(13, 'Country Music', '', 90, 12, '2018-06-09 07:33:34'),
(14, 'Fashion Design', '', 90, 11, '2018-06-09 07:33:34'),
(15, 'Film Scoring', '', 90, 10, '2018-06-09 07:33:34'),
(16, 'Comedy', '', 90, 9, '2018-06-09 07:33:34'),
(17, 'Writing for Television', '', 90, 8, '2018-06-09 07:33:34'),
(18, 'Filmmaking', '', 90, 7, '2018-06-09 07:33:34'),
(19, 'Dramatic Writing', '', 90, 6, '2018-06-09 07:33:34'),
(20, 'Screenwriting', '', 90, 5, '2018-06-09 07:33:34'),
(21, 'Electronic Music Production', '', 90, 4, '2018-06-09 07:33:34'),
(22, 'Cooking #2', '', 90, 3, '2018-06-09 07:33:34'),
(23, 'Shooting, Ball Handler, and Scoring', '', 90, 2, '2018-06-09 07:33:34'),
(24, 'Photography', '', 90, 1, '2018-06-09 07:33:34'),
(25, 'Database System Concept', '', 30, NULL, '2018-06-09 07:33:34'),
(26, 'JavaScript for Beginner', '', 20, NULL, '2018-06-09 07:33:34'),
(27, 'OWASP Top 10', '', 75, NULL, '2018-06-09 07:33:34');

-- --------------------------------------------------------

--
-- Table structure for table `enrolls`
--

CREATE TABLE `enrolls` (
  `student_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `enrolled_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `instructors`
--

CREATE TABLE `instructors` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `instructors`
--

INSERT INTO `instructors` (`id`, `name`, `created_at`) VALUES
(1, 'Annie Leibovitz', '2018-06-09 07:33:34'),
(2, 'Stephen Curry', '2018-06-09 07:33:34'),
(3, 'Gordon Ramsay', '2018-06-09 07:33:34'),
(4, 'Deadmau5', '2018-06-09 07:33:34'),
(5, 'Aaron Sorkin', '2018-06-09 07:33:34'),
(6, 'David Mamet', '2018-06-09 07:33:34'),
(7, 'Werner Herzog', '2018-06-09 07:33:34'),
(8, 'Shonda Rhimes', '2018-06-09 07:33:34'),
(9, 'Steve Martin', '2018-06-09 07:33:34'),
(10, 'Hans Zimmer', '2018-06-09 07:33:34'),
(11, 'Marc Jacobs', '2018-06-09 07:33:34'),
(12, 'Reba Mcentire', '2018-06-09 07:33:34'),
(13, 'Herbie Hancock', '2018-06-09 07:33:34'),
(14, 'Christina Aguilera', '2018-06-09 07:33:34'),
(15, 'Frank Gehry', '2018-06-09 07:33:34'),
(16, 'Diane Von Furstenberg', '2018-06-09 07:33:34'),
(17, 'James Patterson', '2018-06-09 07:33:34'),
(18, 'Usher', '2018-06-09 07:33:34'),
(19, 'Serena Williams', '2018-06-09 07:33:34'),
(20, 'Dr. Jane Goodall', '2018-06-09 07:33:34'),
(21, 'Judy Blume', '2018-06-09 07:33:34'),
(22, 'Garry Kasparov', '2018-06-09 07:33:34'),
(23, 'Samuel L. Jackson', '2018-06-09 07:33:34'),
(24, 'Wolfgang Puck', '2018-06-09 07:33:34'),
(25, 'Martin Scorsese', '2018-06-09 07:33:34'),
(26, 'Bob Woofward', '2018-06-09 07:33:34'),
(27, 'Ron Howard', '2018-06-09 07:33:34'),
(28, 'Thomas Keller', '2018-06-09 07:33:34'),
(29, 'Alice Waters', '2018-06-09 07:33:34'),
(30, 'Helen Mirren', '2018-06-09 07:33:34'),
(31, 'Armin Van Buuren', '2018-06-09 07:33:34');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `salary` int(11) NOT NULL,
  `money` int(11) NOT NULL DEFAULT '1000',
  `json_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `salary`, `money`, `json_data`) VALUES
(1, 'Anakin', 'Skywalker', 100000, 400000, '{\"certification\":[{\"name\":\"Java EE\",\"year\":2009},{\"name\":\"CCNA Security\",\"year\":2010},{\"certification\":\"stupid\",\"year\":2018}]}'),
(2, 'Luke', 'Skywalker', 400000, 200000, '{\"certification\":[{\"name\":\".NET Core\",\"year\":2016},{\"name\":\"CCNA Networking\",\"year\":2015}]}'),
(3, 'Storm1', 'Trooper', 90000, 20000, '{\"certification\":[{\"name\":\"Node.JS Enterprise\",\"year\":2016},{\"name\":\"CCNA Cloud\",\"year\":2015}]}'),
(4, 'Storm2', 'Trooper', 90000, 30000, '{\"certification\":[{\"name\":\".NET Core\",\"year\":2016},{\"name\":\"CCNA Networking\",\"year\":2015}]}'),
(5, 'Storm3', 'Trooper', 50000, 40000, '{\"certification\":[{\"name\":\"Node.JS Enterprise\",\"year\":2016},{\"name\":\"CCNA Cloud\",\"year\":2015}]}'),
(6, 'Storm4', 'Trooper', 30000, 50000, '{\"certification\":[{\"name\":\"Java EE\",\"year\":2009},{\"name\":\"CCNA Security\",\"year\":2010}]}');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `courses_name_key` (`name`),
  ADD KEY `teach_by` (`teach_by`),
  ADD KEY `courses_created_at_desc` (`created_at`);

--
-- Indexes for table `enrolls`
--
ALTER TABLE `enrolls`
  ADD PRIMARY KEY (`student_id`,`course_id`),
  ADD KEY `course_id` (`course_id`);

--
-- Indexes for table `instructors`
--
ALTER TABLE `instructors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `instructors_created_at_desc` (`created_at`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `students_created_at_desc` (`created_at`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `instructors`
--
ALTER TABLE `instructors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `courses`
--
ALTER TABLE `courses`
  ADD CONSTRAINT `courses_ibfk_1` FOREIGN KEY (`teach_by`) REFERENCES `instructors` (`id`);

--
-- Constraints for table `enrolls`
--
ALTER TABLE `enrolls`
  ADD CONSTRAINT `enrolls_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`),
  ADD CONSTRAINT `enrolls_ibfk_2` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
