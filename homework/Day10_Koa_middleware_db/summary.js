'use strict'
let fs = require('fs');

function writeDemo1() {
    return new Promise(onPromiseCompleted);
  }
  function onPromiseCompleted(resolve, reject) {
      setTimeout(function() {
            resolve("Success");
      }, 3000);
  }

  function myWriteFile () {
    fs.writeFile('demofile1.txt', 'test', 'utf8', function(err) {
          if (err)
            console.error(err);
          else
            callback("Success");
      });
  }
 
 async function test() {
    try {
        for (let i =0; i<3; i++) {
            console.log(writeDemo1())
        }
        //console.log(await writeDemo1())
    } catch (ex) {
        console.error("failed! : " + ex)
    }
  }
  test();