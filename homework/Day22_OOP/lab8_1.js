let fs = require('fs');
const {Employee} = require('./employee.js');
const {Employee2} = require('./employee.js');
const {CEO} = require('./ceo.js');

let dang = new Employee('Dang','Red', 10000);
let ceo = new CEO('Somchai','Sudlor', 30000);

dang.hello();
console.log(dang.firstname + "'s salary is "+ dang.getSalary());
ceo.hello();
console.log(ceo.getSalary());
console.log(ceo.firstname + "'s salary is "+ ceo.getSalary());