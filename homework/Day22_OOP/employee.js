class Employee {
    constructor(firstname, lastname, salary) {
        let _salary = salary; // simulate private variable

        this.firstname = firstname; // public property
        this.lastname = lastname; // public property
        this.getSalaryPrivate = function () {  // simulate public method
            return _salary;
        }
    }

    getSalary() {
        return this.getSalaryPrivate();
    }
    
    hello() { // simulate public method
        console.log("Hello "+this.firstname+"!");
    }
}

exports.Employee = Employee;