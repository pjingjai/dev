const fs = require('fs');

function hello6(err, value) {
    callbackValue = value;
    console.log(callbackValue); // print callbackValueNew after 3 seconds
}
callbackValue = 'callbackValueOld';
function tryHello6(callbackFunction) {
    let returnValue = 'returnValue';
    let callbackValue = 'callbackValueNew';
    setTimeout(callbackFunction, 3000, null, callbackValue);
    return returnValue;
}
console.log(tryHello6(hello6)); // print returnValue
console.log(callbackValue); // print callbackValueOld
  
  



  
/*
function myReadFile(filename, callbackFunction) {
    fs.readFile(filename, 'utf8', function(err, result) {
        callbackFunction(err, result);
        return result;
    });
}

let a = myReadFile('demofile2.txt', function(err, data) {
    console.log(data);
});

console.log(a);
  */
fs.writeFile('demofile2.txt', 'file2 content', 'utf8', writeFileCompleted);

function writeFileCompleted(err) {
    if (err) {
        console.error(err);
        return;
    }
    fs.readFile('demofile2.txt', 'utf8', readFileCompleted);
}

function readFileCompleted(err, data) {
    if (err) {
        console.error(err);
        return;
    }
    console.log(data);
}

/*

  for (let i=0; i<100; i++) {
    fs.readFile('demofile1.txt', 'utf8', (err, data) => {
        console.log(data);
    });
    
    fs.readFile('demofile2.txt', 'utf8', (err, data) => {
        console.log(data);
    });
  }
*/